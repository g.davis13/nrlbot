# /r/NRL Match Thread Bot

A work in progress. There's nothing to see here right now, unless you are interested in contributing to development.

## Status

- **Current Version (0.3.0)**:
 - Responds to "nrlbot streams" in an /r/nrl match thread with a list of live streams pulled from livestream.com.
- **Known Issues**:
 - Livestream playlists contain duplicate streams; currently these duplicates are not removed prior to generating output.

## Development Roadmap

This began as an experiment to see if we could automatically extract the livestream streams for NRL matches. I'd like to turn this into a call/response bot (see: https://praw.readthedocs.io/en/stable/pages/call_and_response_bot.html) for use in /r/nrl match threads. For example, if a user was looking for links to the streams for the match, they could post a comment in the match thread, containing ```nrlbot streams```. The bot would reply with links to streams for the game. Other features might be providing stats such as the current score (```nrlbot score```), penalty count (```nrlbot penalties```), player's match stats, etc.

## Installation

I will be attempting to make these installation instructions as simple as possible, so that people from the /r/nrl community who might not have much experience doing development can contribute. Unfortuntely, most of the documentation you will find online for Python 3, git, virtualenv, etc assumes you are using Linux or a Unix variant (such as OS X). If you don't do a lot of development, you're probably using Windows, so I will focus on Windows for these instructions.

### Requirements

You should have a working installation of Python 3, git, and virtualenv.

#### Python 3 (Windows)

- Go to https://www.python.org/downloads/windows/ and download the latest Python 3 release.
- Run the installer. All the defaults should be fine. Make sure that 'Add python to PATH' option is checked.
- Test your Python installation. Open a command shell (Start -> Run -> 'cmd') and type ```python --version```. This should output something like _'Python 3.5.2'_

#### virtualenv (Windows/Unix)

- virtualenv is a tool to create isolated Python environments, see: https://virtualenv.pypa.io
- pip is the standard way to install Python packages, and is included with all recent versions of Python on all major platforms.
- In a command shell, type ```pip install virtualenv``` to install the lastest version of virtualenv.

#### git (Windows)
- git is a version control system, allowing people to collaborate on the same codebase by merging changes, managing versions, etc.
- Visit https://git-scm.com/download/win and download the latest version.
- Run the installer. All the defaults should be fine.
- Test your git installation. Open a command shell (Start -> Run -> 'cmd') and type ```git --version```. This should output something like _'git version 2.9.2'_.


### Get the code

- At a command shell, run ```git clone https://gitlab.com/g.davis13/nrlbot.git```.
This will create a sub-directory 'nrlbot' in the current working directory, containing the latest project release.
- Change into the project directory: ```cd nrlbot```
- Keep this shell open for the next step.

### Create a virtual environment & install dependancies

- Type ```virtualenv env```. This will create a virtualenv in the sub-directory 'env'.
- Activate the virtualenv (Windows): ```env\Scripts\activate```
- Activate the virtualenv (Linux): ```source ./env/bin/activate```
- Install dependancies: ```pip install -r requirements.txt ```

## Usage

- Change into the 'nrlbot' project directory
- Activate the virtualenv (Windows): ```env\Scripts\activate```
- Activate the virtualenv (Linux): ```source ./env/bin/activate```
- Run the script: ```python3 nrlbot.py [username] [password]```
- Output can be customised by editing 'post.txt' in the project directory. Templating is via jinja2, see templating guide here: http://jinja.pocoo.org/docs/dev/templates/
