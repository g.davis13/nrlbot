#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  livestream.py
#
#  Copyright 2016 G. Davis <g.davis13+nrlbot@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

"""Generic Livestream API Library for /r/NRL Match Thread Assistant Bot.

Provides a thin wrapper around the (undocumented) Livestream REST API. Allows
programtic access to public Livestream events.

TODO:
- better errors - more specific and descriptive
"""

import arrow
import datetime
import requests

HOST = 'http://api.new.livestream.com/'
# ACCOUNT = '3161248'
TIMEOUT = 60
HEADERS = {
    'host': "api.new.livestream.com",
    'connection': "keep-alive",
    'accept': "*/*",
    'origin': "http://livestream.com",
    'x-requested-with': "XMLHttpRequest",
    'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.57 Safari/537.36",  # noqa
    'dnt': "1",
    'referer': "http://livestream.com/nrl",
    'accept-encoding': "gzip, deflate, sdch",
    'accept-language': "en-AU,en;q=0.8,en-US;q=0.6",
    'cache-control': "no-cache",
    }


class LivestreamError(Exception):
    """Base class for exceptions in this module."""

    pass


class ResponseCodeError(LivestreamError):
    """Exception raised for errors in the input.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        """."""
        self.message = message


class NetworkError(LivestreamError):
    """Exception raised for errors in the input.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        """."""
        self.message = message


def api_request(endpoint, headers, timeout):
    """Generic API request.

    Make a GET request to the specified endpoint and return the JSON data.

    Args:
        endpoint: (string) URL of the API endpoint to send the request.
        headers: (dict) URL of the API endpoint to send the request.
        timeout: (int) time in seconds to wait for a response before failing

    Returns:
        A dictionary representing JSON component of the HTTP response.

    Raises:
        ResponseCodeError: the API server responded with a non-2xx HTTP
            status code.
        NetworkError: a connectivity error caused the request to fail. May
            be triggered by requests.Timeout, requests.ConnectionError, or
            requests.TooManyRedirects
    """
    try:
        r = requests.get(endpoint)
        r.raise_for_status()
    except requests.HTTPError:
        msg = 'The server returned a non-2xx HTTP status code ({})'\
            .format(r.status_code)
        raise ResponseCodeError(msg)
    except (requests.Timeout, requests.ConnectionError,
            requests.TooManyRedirects):
        msg = 'A network error occured'
        raise NetworkError(msg)
    return r.json()


class Playlist(object):
    """Represents a Livestream event's playlist/streams."""

    pass


class Event(object):
    """Represents a Livestream event.

    Attributes:
        id:             Livestream event ID.
        short_name:     shortened event name (suitable for use as a URL slug).
        full_name:      complete event name (suitable for display).
        description:    description/details of event.
        account_id:     ID of event producer account.
        url:            the API URL for this event.
        start_time:     datetime for the scheduled event start time.
        end_time:       datetime for the scheduled event end time.
        is_live:        true if the event is currently live, otherwise false.
        m3u8_url:       URL to event playlist (.m3u8 file).
    """

    def __init__(self, event_id, account_id, data={}, refresh=60, host=HOST,
                 headers=HEADERS, timeout=TIMEOUT):
        """Init Event object.

        Args:
            event_id:   Livestream ID for this event
            account_id: Livestream owner account ID for this event
            data:       dictionary of Event data retrieved from API (optional)
            refresh:    amount of time (in seconds) after which internal data
                            is considered stale (optional, default=60)
            host:       fully qualified API host
                            eg: "http://api.new.livestream.com/"
            headers:    dictionary of event headers
            timeout:    timeout in seconds
        """
        self.id = event_id
        self.account_id = account_id
        self._data = data
        self.refresh = refresh
        self._host = host
        self._headers = headers
        self._timeout = timeout
        self.last_loaded = datetime.datetime.now().timestamp()

    @property
    def age(self):
        """Age of ._data (in seconds)."""
        return int(datetime.datetime.now().timestamp() - self.last_loaded)

    @property
    def is_stale(self):
        """True if interval > than 'refresh' has passed since last 'data' load.

        Checks if the delta between the current time and the time since the
        data was last loaded from the API is greater than the time set in
        the 'refresh' attribute. Also returns true if '_data' is empty.
        """
        if not self._data or self.age > self.refresh:
            return True
        return False

    @property
    def data(self):
        """Access the raw event data.

        If ._data is empty or stale, reload before returning.
        """
        if self.is_stale:
            self.reload()
        return self._data

    @property
    def short_name(self):
        """."""
        return self.data['short_name']

    @property
    def full_name(self):
        """."""
        return self.data['full_name']

    @property
    def description(self):
        """."""
        return self.data['description']

    @property
    def url(self):
        """Return the API URL for this event."""
        return '{}accounts/{}/events/{}'.format(self._host, self.account_id,
                                                self.id)

    @property
    def start_time(self):
        """Scheduled start time for the event.

        Returns a (UTC) datetime object.
        """
        d = arrow.get(self.data['start_time']).date()
        t = arrow.get(self.data['start_time']).timetz()
        return datetime.datetime.combine(d, t)

    @property
    def end_time(self):
        """Scheduled start time for the event.

        Returns a (UTC) datetime object.
        """
        d = arrow.get(self.data['end_time']).date()
        t = arrow.get(self.data['end_time']).timetz()
        return datetime.datetime.combine(d, t)

    def countdown(self):
        """A humanized string, remaining time until the event commences."""
        present = arrow.now()
        start_time = arrow.get(self.start_time)
        return start_time.humanize(present)

    @property
    def is_live(self):
        """Return True if the event is live, or False."""
        is_live = False
        # the stream_info attribute only exists while an event is live
        try:
            is_live = self.data['stream_info']['is_live']
        except (KeyError, TypeError):
            pass
        return is_live

    @property
    def m3u8_url(self):
        """Return the event playlist URL, or None."""
        playlist = None
        # the stream_info attribute only exists while an event is live
        try:
            playlist = self.data['stream_info']['m3u8_url']
        except (KeyError, TypeError):
            pass
        return playlist

    def reload(self):
        """Re-request the Event data from the API."""
        self._data = api_request(self.url, self._headers, self._timeout)
        self.last_loaded = datetime.datetime.now().timestamp()
        return


class Account(object):
    """Represents a Livestream producer account.

    Currently, Account objects are only useful for generating Events.

    Attributes:
        id:             Livestream account ID.
        events:         a list of events belonging to this account
        host:           Livestream API host
    """

    def __init__(self, account_id, host=HOST, headers=HEADERS,
                 timeout=TIMEOUT):
        """Init Account object.

        Args:
            account_id:     Livestream account ID
            host:           fully qualified API host
                                eg: "http://api.new.livestream.com/"
            headers:        dictionary of event headers
            timeout:        timeout in seconds
        """
        self.id = account_id
        self.host = host
        self._headers = headers
        self._timeout = timeout

    @property
    def url(self):
        """API URL for this account."""
        return '{}accounts/{}'.format(self.host, self.id)

    def events(self, live=False):
        """Fetch a list of Livestream events belonging to this account.

        Query the Livestream API for the list of events belonging to the
        Livestream Account ID set in this class instance. If live is true, will
        only return events that are currently in progress. Otherwise, the list
        will include historical and future events.

        Args:
            live: (bool) If true, will return only events currently in
                progress.
            timeout: (real?) amount of time in seconds to wait for a response.

        Returns:
            A list of Event objects.
        """
        endpoint = self.url + '/events/'
        response = api_request(endpoint, self._headers, self._timeout)
        events = [Event(e['id'], self.id, e) for e in response["data"]]
        if live:
            events = [e for e in events if e.is_live]
        return events

    def event(self, event_id):
        """Fetch a single Livestream event belonging to this account.

        Query the Livestream API for the event event_id.

        Args:
            event_id: (int) livestream event id
            timeout: (real?) amount of time in seconds to wait for a response.

        Returns:
            An Event object.
        """
        endpoint = '{}/events/{}'.format(self.url, event_id)
        event_data = api_request(endpoint)
        return Event(event_id, self.account_id, event_data)
